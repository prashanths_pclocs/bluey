import noble from '@abandonware/noble';
import { Observable, filter, first, firstValueFrom, fromEvent, fromEventPattern, map, shareReplay, tap } from 'rxjs';
import * as readline from 'readline';

export function nobleEventPattern<T extends unknown>(pattern: string): Observable<T> {
  return fromEventPattern(handler => noble.on(pattern, handler), handler => noble.removeListener(pattern, handler));
}

const REAL_TOWER_UUID = '8bd32aae730e75687b8103f557df4ca7';

const SERVICE_ID = 'fffffffffffffffffffffffffffffff0';

const MSG_CHARACTERISTIC_ID = 'fffffffffffffffffffffffffffffff1';
const ONBOARDING_CHARACTERISTIC_ID = 'fffffffffffffffffffffffffffffff2';



async function messagePassing(tower: noble.Peripheral) {
  const servicesAndChars = await tower.discoverSomeServicesAndCharacteristicsAsync([SERVICE_ID], [MSG_CHARACTERISTIC_ID]);
  const characteristic = servicesAndChars.characteristics[0];

  const data = await characteristic.readAsync();
  console.log('>>> ', data.toString());


  console.log('Subscribing to characteristic');
  await characteristic.subscribeAsync();
  fromEvent<Buffer>(characteristic, 'data').pipe(
    map(([v]) => v.toString()),
  ).subscribe(v => {
    console.log('>>> ', v.toString());
  });

  const stdIn = readline.createInterface({ input: process.stdin });

  while (true) {
    console.log('Enter data to send: ');
    const dataToSend = await firstValueFrom(fromEvent<string>(stdIn, 'line'));
    console.log('<<< ', dataToSend);
    await characteristic.writeAsync(Buffer.from(dataToSend), false);
  }
}



async function setWifi(tower: noble.Peripheral) {
  const servicesAndChars = await tower.discoverSomeServicesAndCharacteristicsAsync([SERVICE_ID], [ONBOARDING_CHARACTERISTIC_ID]);
  const characteristic = servicesAndChars.characteristics[0];

  console.log('Reading characteristic');
  const data = await characteristic.readAsync();
  console.log('Char data', data.toString());

  const statusStream = fromEvent<Buffer>(characteristic, 'data').pipe(
    map(([v]) => {
      try {
        return JSON.parse(v.toString()) as OnboardingStatus;
      } catch (e) {
        console.error(`Failed to parse ${v.toString()}`, e);
      }
    }),
    shareReplay(1),
  );
  await characteristic.subscribeAsync();

  const writeRequest: OnboardingWriteRequest = {
    accountCode: 'LINKME',
    networkSettings: {
      wifi: {
        dhcp: 'both',
        wpaSupplicantConfig: {
          enabled: true,
          keyMgmt: 'WPA-PSK',
          ssid: 'lazers',
          password: 'pclocs123',
        }
      }
    }
  };

  console.log('sending up new settings');

  void characteristic.write(Buffer.from(JSON.stringify(writeRequest)), false);


  console.log('waiting for a status back');
  const resultingStatus = await firstValueFrom(statusStream.pipe(
    tap(v => {
      console.log('got status', v);
    }),
    filter(v => v?.isApplying === false),
  ));


  console.log('final result', resultingStatus);

  await characteristic.unsubscribeAsync();
}






async function bootstrap() {
  console.log('starting');

  noble.on('stateChange', state => console.log)


  await firstValueFrom(nobleEventPattern('stateChange').pipe(
    filter((state) => state === 'poweredOn'),
    first()
  ));

  console.log('noble powered on');


  void noble.startScanningAsync();

  const tower = await firstValueFrom(nobleEventPattern<noble.Peripheral>('discover').pipe(
    tap(v => v.advertisement.serviceUuids),
    filter(v => v.advertisement.serviceUuids?.includes(SERVICE_ID) && v.uuid === REAL_TOWER_UUID),
  ));


  await noble.stopScanningAsync();

  await tower.connectAsync();

  await messagePassing(tower);
}

void bootstrap();






interface WpaSupplicantConfig {
  enabled: boolean;
  ssid: string;
  keyMgmt: 'NONE' | 'WPA-PSK' | 'WPA-EAP';
  /** This should always be a hashed PSK */
  psk?: string;
  privateKey?: string;
  clientCert?: string;
  caCert?: string;
  identity?: string;
  password?: string;
  eap?: 'TLS' | 'PEAP';
  scanSsid?: 1;
}

interface NetworkInterfaceSettings {
  dhcp: 'none' | 'both' | 'ipv4' | 'ipv6';
  address?: string[];
  gateway?: string[];
  dns?: string[];
  wpaSupplicantConfig?: WpaSupplicantConfig;
}

interface OnboardingWriteRequest {
  accountCode: string;
  networkSettings: {
    ethernet?: NetworkInterfaceSettings;
    wifi?: NetworkInterfaceSettings;
  };
}



interface OnboardingStatus {
  isApplying: boolean;
  isDone: boolean; // network connected (IoT, AND linked to account)
  errors: {
    timeSync: boolean;
    dnsResolved: boolean;
    ssl: boolean;
    certDuplicate: boolean;
    iot: boolean;
    accountLinked: boolean;
  }
}

